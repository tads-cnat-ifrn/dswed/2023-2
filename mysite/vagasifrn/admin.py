from django.contrib import admin
from .models import Servidor, InstituicaoParceira, Curso, Vagas

admin.site.register(Servidor)
admin.site.register(InstituicaoParceira)
admin.site.register(Curso)
admin.site.register(Vagas)