# Generated by Django 4.0.6 on 2023-12-26 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vagasifrn', '0002_instituicaoparceira_user_servidor'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='instituicaoparceira',
            options={'verbose_name_plural': 'instituições parceiras'},
        ),
        migrations.AlterModelOptions(
            name='servidor',
            options={'verbose_name_plural': 'servidores'},
        ),
        migrations.AddField(
            model_name='vagas',
            name='remuneracao',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
    ]
