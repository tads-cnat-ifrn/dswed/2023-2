from django.contrib.auth.models import User
from django.contrib.auth import login
from .models import Curso, Vagas, VagasForm, InstituicaoParceira
from django.contrib import messages

####
## Classe CursoService
######################
class CursoService():
    def get_all(self):
        return Curso.objects.all()

    def add_curso(self, request):
        nome_val = request.POST.get('nome', None)
        area_val = request.POST.get('area', None)
        if nome_val and area_val:
            Curso.objects.create(nome = nome_val, area = area_val)
            messages.success(request, 'Curso cadastrado com sucesso!')
            return True
        else:
            messages.error(request, 'Nessário preencher todos os campos!')
        return False

####
## Classe VagasService
######################
class VagasService():
    def get_all(self):
        return Vagas.objects.all()

    def add_vaga(self, request):
        form = VagasForm(request.POST, request.FILES)
        if form.is_valid():
            vaga = form.save(commit = False)
            vaga.instituicao_parceira = request.user.instituicaoparceira
            vaga.save()
            form.save_m2m()
            messages.success(request, 'Vaga cadastrada com sucesso!')
            return None
        messages.error(request, 'Erro na validação dos campos preenchidos')
        return form

    def edit_vaga(self, request, kwargs):
        pk = kwargs.get('pk', -1)
        try:
            vaga = Vagas.objects.get(pk=pk)
        except (Vagas.DoesNotExist):
            messages.error(request, 'Identificação de vaga inválida!')
        if vaga != None:
            form = VagasForm(request.POST, request.FILES, instance=vaga)
            if form.is_valid():
                form.save()
                messages.success(request, 'Vaga atualizada com sucesso!')
                return None
            else:
                messages.error(request,
                    'Erro na atualização de vaga! Preencha todos os campos.'
                )
        form = VagasForm(request.POST, request.FILES)
        return form

    def get_vaga(self, kwargs):
        pk = kwargs.get('pk', -1)
        try:
            vaga = Vagas.objects.get(pk=pk)
        except (Vagas.DoesNotExist):
            vaga = None
        return vaga

    def valida_vaga(self, request, kwargs):
        pk = kwargs.get('pk', -1)
        try:
            vaga = Vagas.objects.get(pk=pk)
            vaga.validada = True
            vaga.save()
            messages.success(request, 'Vaga validada com sucesso!')
        except (Vagas.DoesNotExist):
            messages.error(request, 'Vaga informada não localizada!')


####
## Classe InstituicaoSercice
############################
class InstituicaoService():
    def possui_validas(self):
        return InstituicaoParceira.objects.filter(eh_valida = True).count() > 0

    def get_validas(self):
        return InstituicaoParceira.objects.filter(eh_valida = True)

    def possui_invalidas(self):
        return InstituicaoParceira.objects.filter(eh_valida = False).count() > 0

    def get_invalidas(self):
        return InstituicaoParceira.objects.filter(eh_valida = False)

    def add_instituicao(self, request):
        nome = request.POST.get('nome', '')
        endereco = request.POST.get('end', '')
        username = request.POST.get('login', '')
        senha1 = request.POST.get('senha1', '')
        senha2 = request.POST.get('senha2', '')
        email = request.POST.get('email', '')
        if nome and endereco and username and senha1 and email:
            if senha1 == senha2:
                usr = User.objects.create_user(username, email, senha1)
                InstituicaoParceira.objects.create(
                    nome=nome, endereco=endereco, user=usr
                )
                messages.success(request, 'Instituição cadastrada com sucesso!')
                login(request, usr)
                return True
            else:
                messages.error(request, 'Senhas informadas precisam ser iguais!')
                return False
        else:
            messages.error(request, 'Todos parâmetros precisam ser preenchidos!')
            return False