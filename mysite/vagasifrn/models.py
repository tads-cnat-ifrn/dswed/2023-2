from django.contrib.auth.models import User
from django.db import models
from django.forms import ModelForm, Textarea, CheckboxSelectMultiple, DateInput

## Classe representando um servidor da DIREX
##-------------------------------------------
class Servidor(models.Model):
    class Meta():
        verbose_name_plural = 'servidores'
    matricula = models.IntegerField()
    user = models.OneToOneField(
        User, unique=True, null=True, on_delete=models.SET_NULL
    )
    def __str__(self):
        return self.matricula

## Classe representando uma instituição parceira
##-----------------------------------------------
class InstituicaoParceira(models.Model):
    class Meta():
        verbose_name_plural = 'instituições parceiras'
    nome = models.CharField(max_length = 40, unique = True)
    endereco = models.CharField(max_length = 100)
    eh_valida = models.BooleanField(default=False)
    user = models.OneToOneField(
        User, unique=True, null=True, on_delete=models.SET_NULL
    )
    def __str__(self):
        return self.nome
    def possui_vagas(self):
        return self.vagas_set.count() > 0
    def vagas_a_validar(self):
        return self.vagas_set.filter(validada = False).count()
    def vagas_validas(self):
        return self.vagas_set.all().filter(validada = True)
    def vagas_nao_validas(self):
        return self.vagas_set.all().filter(validada = False)

## Classe representando um curso da instituição
##----------------------------------------------
class Curso(models.Model):
    AREA = [
        ('gti', 'Gestão e Tencologia da Informação'),
        ('cci', 'Construção Civil'),
        ('ind', 'Indústria'),
    ]
    nome = models.CharField(max_length = 20, unique = True)
    area = models.CharField(max_length = 3, choices = AREA)
    def __str__(self):
        return self.nome

## Classe representando uma publicação de vagas de uma dada instituição
##----------------------------------------------------------------------
class Vagas(models.Model):
    instituicao_parceira = models.ForeignKey(
        InstituicaoParceira, on_delete = models.CASCADE
    )
    cursos = models.ManyToManyField(Curso, verbose_name='Cursos associados')
    funcao = models.CharField('Função', max_length = 50)
    prerequisitos = models.CharField('Pré-requisitos', max_length = 200)
    num_vagas = models.IntegerField('Número de vagas', default = 1)
    vagas_ocupadas = models.IntegerField(default = 0)
    banner = models.ImageField(upload_to = 'vagas', null = True, blank = True)
    data_pub = models.DateField('Data de publicação', auto_now_add = True)
    data_limite = models.DateField(null = True, blank = True)
    validada = models.BooleanField(default = False)
    remuneracao = models.DecimalField('Remuneração', max_digits=10, decimal_places=2)
    def preenchida(self):
        return self.num_vagas == self.vagas_ocupadas
    def disponivel(self):
        return self.num_vagas - self.vagas_ocupadas

class VagasForm(ModelForm):
    class Meta:
        model = Vagas
        fields = [
            'funcao', 'remuneracao', 'prerequisitos', 'num_vagas',
            'cursos', 'banner', 'data_limite'
        ]
        widgets = {
            'prerequisitos': Textarea(attrs={"cols": 70, "rows": 3}),
            'cursos': CheckboxSelectMultiple(),
            'data_limite': DateInput(attrs=dict(type='date'))
        }

