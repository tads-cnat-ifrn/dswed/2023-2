from django.apps import AppConfig


class VagasifrnConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vagasifrn'
