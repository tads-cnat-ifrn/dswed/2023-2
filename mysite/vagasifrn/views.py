from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404
from django.views import View
from .services import CursoService, VagasService, InstituicaoService
from .models import VagasForm

####
## Testes para usuários logados
################################
def eh_parceira(user):
    return hasattr(user, 'instituicaoparceira')

def eh_parceira_valida(user):
    if hasattr(user, 'instituicaoparceira'):
        return user.instituicaoparceira.eh_valida
    return False

def eh_servidor(user):
    return hasattr(user, 'servidor')

####
## Classes de Visão
####################

## Classe para adição de uma nova instituição
##--------------------------------------------
class NovaInstituicaoView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'vagasifrn/form_instituicao.html')

    def post(self, request, *args, **kwargs):
        ips = InstituicaoService()
        inserida = ips.add_instituicao(request)
        if inserida:
            return redirect('vagasifrn:instituicao')
        return render(request, 'vagasifrn/form_instituicao.html')

## Classe para visualização de DETALHES de uma dada vaga
##-------------------------------------------------------
class DetalhesVagaView(View):
    def get(self, request, *args, **kwargs):
        vs = VagasService()
        vaga = vs.get_vaga(kwargs)
        return render(request, 'vagasifrn/vaga.html', {'vaga': vaga})

## Clase para ADIÇÃO de uma nova vaga
##------------------------------------
@method_decorator(user_passes_test(eh_parceira), name='dispatch')
class NovaVagaView(View):
    def get(self, request, *args, **kwargs):
        form = VagasForm()
        return render(request, 'vagasifrn/form_vaga.html', {'form': form})

    def post(self, request, *args, **kwargs):
        vs = VagasService()
        form = vs.add_vaga(request)
        if form == None:
            return redirect('vagasifrn:instituicao')
        return render(request, 'vagasifrn/form_vaga.html', {'form': form})

## Clase para EDIÇÃO de vaga cadastrada
##--------------------------------------
@method_decorator(user_passes_test(eh_parceira), name='dispatch')
class EdicaoVagaView(View):
    def get(self, request, *args, **kwargs):
        vs = VagasService()
        vaga = vs.get_vaga(kwargs)
        if vaga == None:
            raise Http404('Identificação de vaga inválida!')
        form = VagasForm(instance = vaga)
        contexto = {'form': form, 'edit': True, 'vaga_id': vaga.id}
        return render(request, 'vagasifrn/form_vaga.html', contexto)

    def post(self, request, *args, **kwargs):
        vs = VagasService()
        form = vs.edit_vaga(request, kwargs)
        if form == None:
            return redirect('vagasifrn:instituicao')
        vaga_id = kwargs.get('pk', -1)
        contexto = {'form': form, 'edit': True, 'vaga_id': vaga_id}
        return render(request, 'vagasifrn/form_vaga.html', contexto)

## Classe que identifica o tipo de usuário logado
##------------------------------------------------
@method_decorator(login_required, name='dispatch')
class DefinePerfilView(View):
    def get(self, request, *args, **kwargs):
        if hasattr(request.user, 'servidor'):
            return redirect('vagasifrn:direx')
        elif hasattr(request.user, 'instituicaoparceira'):
            return redirect('vagasifrn:instituicao')
        messages.error(request, 'Usuário logado é inválido, use um usuário válido!')
        return redirect('login')

## Classe que apresenta a tela inicial para servidores da DIREX
##--------------------------------------------------------------
@method_decorator(user_passes_test(eh_servidor), name='dispatch')
class PainelExtensaoView(View):
    def get(self, request, *args, **kwargs):
        ips = InstituicaoService()
        return render(request, 'vagasifrn/direx.html', {'service': ips})

## Classe responsável para tornar uma vaga "válida"
##--------------------------------------------------
@method_decorator(user_passes_test(eh_servidor), name='dispatch')
class ValidaVagaView(View):
    def get(self, request, *args, **kwargs):
        vs = VagasService()
        vs.valida_vaga(request, kwargs)
        return redirect('vagasifrn:direx')


## Classe que apresenta a tela inicial para empresas parceiras
##-------------------------------------------------------------
@method_decorator(user_passes_test(eh_parceira), name='dispatch')
class PainelEmpresaView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'vagasifrn/instituicao.html')

## Classe que apresenta a tela inicial da aplicação - vagas caadastradas
##-----------------------------------------------------------------------
class VagasView(View):
    def get(self, request, *args, **kwargs):
        vs = VagasService()
        vagas = vs.get_all()
        return render(request, 'vagasifrn/index.html', {'vagas': vagas})

## Classe que apresenta os cursos cadastrados
##--------------------------------------------
@method_decorator(user_passes_test(eh_servidor), name='dispatch')
class CursosView(View):
    def get(self, request, *args, **kwags):
        cs = CursoService()
        cursos = cs.get_all()
        return render(request, 'vagasifrn/cursos.html', {'cursos': cursos})

## Classe responsável pelo cadastramento de um novo curso
##--------------------------------------------------------
@method_decorator(user_passes_test(eh_servidor), name='dispatch')
class NovoCursoView(View):
    def get(self, request, *args, **kwags):
        return render(request, 'vagasifrn/form_curso.html')

    def post(self, request, *args, **kwags):
        cs = CursoService()
        inserido = cs.add_curso(request)
        if inserido:
            return HttpResponseRedirect(reverse('vagasifrn:lista_cursos'))
        else:
            return render(request, 'vagasifrn/form_curso.html')





