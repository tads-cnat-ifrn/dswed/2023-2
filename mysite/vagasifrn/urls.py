from django.urls import path
from . import views

app_name = 'vagasifrn'
urlpatterns = [
    path('', views.VagasView.as_view(), name='index'),
    path('perfil/', views.DefinePerfilView.as_view(), name='perfil'),
    path('direx/', views.PainelExtensaoView.as_view(), name='direx'),
    path('instituicao/', views.PainelEmpresaView.as_view(), name='instituicao'),
    path('vaga/add/', views.NovaVagaView.as_view(), name='vaga_add'),
    path('vaga/<int:pk>/', views.DetalhesVagaView.as_view(), name='vaga'),
    path('vaga/<int:pk>/edit/', views.EdicaoVagaView.as_view(), name='vaga_edit'),
    path('vaga/<int:pk>/valida/', views.ValidaVagaView.as_view(), name='vaga_valid'),
    path('cursos/', views.CursosView.as_view(), name='lista_cursos'),
    path('cursos/add/', views.NovoCursoView.as_view(), name='curso_add'),
    path(
        'instituicao/add/',
        views.NovaInstituicaoView.as_view(), name='instituicao_add'
    ),
]