from django.shortcuts import render, get_object_or_404
from django.views import View
from .models import Prova

class ProvaView(View):
    def get(self, request, *args, **kwargs):
        prova_id = kwargs.get('prova_id', -1)
        prova = get_object_or_404(Prova, pk=prova_id)
        contexto = {'prova': prova}
        return render(request, 'prova1/prova.html', contexto)
