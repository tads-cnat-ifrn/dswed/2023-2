from django.contrib import admin
from .models import Aluno, Prova, Questao, Alternativa, ProvaRealizada, Resposta

class AlunoAdmin(admin.ModelAdmin):
    list_display = ['nome', 'id', 'matricula']

class ProvaAdmin(admin.ModelAdmin):
    list_display = ['nome', 'id']

class QuestaoAdmin(admin.ModelAdmin):
    list_display = ['prova', 'id', 'enunciado', 'pontos']

class AlternativaAdmin(admin.ModelAdmin):
    list_display = ['questao', 'id', 'texto', 'correta']

class ProvaRealizadaAdmin(admin.ModelAdmin):
    list_display = ['data', 'aluno', 'prova']

admin.site.register(Aluno, AlunoAdmin)
admin.site.register(Prova, ProvaAdmin)
admin.site.register(Questao, QuestaoAdmin)
admin.site.register(Alternativa, AlternativaAdmin)
admin.site.register(ProvaRealizada, ProvaRealizadaAdmin)
admin.site.register(Resposta)