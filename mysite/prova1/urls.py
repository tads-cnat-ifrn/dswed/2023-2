from django.urls import path
from . import views

urlpatterns = [
    path('<int:prova_id>/resultados/', views.ProvaView.as_view(), name='prova'),
]