from django.db import models

class Aluno(models.Model):
    nome = models.CharField(max_length=40)
    matricula = models.CharField(max_length=15)
    def __str__(self):
        return '({}) {}'.format(self.id, self.nome)

class Prova(models.Model):
    nome = models.CharField(max_length=30)
    def soma_pontos(self):
        soma = 0
        for q in self.questao_set.all():
            soma += q.pontos
        return soma
    def __str__(self):
        return '({}) {}'.format(self.id, self.nome)

class Questao(models.Model):
    ordem = models.IntegerField()
    enunciado = models.CharField(max_length=300)
    pontos = models.IntegerField(default=10)
    prova = models.ForeignKey(Prova, on_delete=models.CASCADE)
    def __str__(self):
        return '({}) {}'.format(self.id, self.enunciado)

class Alternativa(models.Model):
    texto = models.CharField(max_length=300)
    correta = models.BooleanField(default=False)
    questao = models.ForeignKey(Questao, on_delete=models.CASCADE)
    def __str__(self):
        return '({}) questão: {} = {}'.format(
            self.id, self.questao.enunciado, self.texto
        )

class ProvaRealizada(models.Model):
    data = models.DateTimeField()
    aluno = models.ForeignKey(Aluno, on_delete=models.CASCADE)
    prova = models.ForeignKey(Prova, on_delete=models.CASCADE)
    def pontos_obtidos(self):
        total = 0
        for resp in self.resposta_set.all():
            total += resp.pontos()
        return total
    def __str__(self):
        return '{} - por: {}'.format(self.prova, self.aluno)

class Resposta(models.Model):
    prova = models.ForeignKey(ProvaRealizada, on_delete=models.CASCADE)
    alternativa = models.ForeignKey(Alternativa, on_delete=models.CASCADE)
    def pontos(self):
        if self.alternativa.correta:
            return self.alternativa.questao.pontos
        return 0
    def __str__(self):
        return '({}) resposta: {} - questão: {}'.format(
            self.id, self.alternativa.texto, self.alternativa.questao
        )

