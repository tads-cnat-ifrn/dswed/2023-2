import datetime
from django.db import models
from django.utils import timezone

class Pergunta(models.Model):
    texto = models.CharField(max_length=150)
    data_pub = models.DateTimeField('Data de publicação')
    ativo = models.BooleanField(default=True)
    def __str__(self):
        return '{} ({})'.format(self.texto, self.id)
    def publicada_recentemente(self):
        agora = timezone.now()
        return ((self.data_pub <= agora) and
            (self.data_pub >= agora - datetime.timedelta(hours=24)))
    publicada_recentemente.admin_order_field = 'data_pub'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'Recente?'
    def eh_valida(self):
        if self.alternativa_set.count() > 1:
            return True
        return False

class Alternativa(models.Model):
    texto = models.CharField(max_length=80)
    quant_votos = models.IntegerField('Quantidade de votos', default=0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    ativo = models.BooleanField(default=True)
    def __str__(self):
        return '{} - {} ({})'.format(self.pergunta.texto, self.texto, self.id)