import datetime
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from .models import Pergunta
'''
    TETES FUNCIONAIS
'''
def cria_pergunta(texto, dias):
    """
    Cria uma pergunta com um texto e um delta de tantos dias (futuro/passado)
    """
    data = timezone.now() + datetime.timedelta(days=dias)
    return Pergunta.objects.create(texto=texto, data_pub=data)

###
### Testes para a classe IndexView
##################################
class IndexViewTestes(TestCase):
    def test_sem_perguntas(self):
        """
        Não havendo perguntas DEVE ser exibida a mensagem informativa
        """
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma enquete cadastrada")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_com_pergunta_no_passado(self):
        """
        Perguntas com data de publicação no passado são exibidas normalmente
        """
        cria_pergunta(texto='Pergunta no passado', dias=-30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'],
            ['<Pergunta: Pergunta no passado (1)>']
        )

    def test_com_pergunta_no_futuro(self):
        """
        Perguntas com data de publicação no futuro NÃO são exibidas na Index
        """
        cria_pergunta(texto="Pergunta no futuro", dias=30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertContains(resposta, "Nenhuma enquete cadastrada")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_pergunta_no_passado_e_pergunta_no_futuro(self):
        """
        Exibe as perguntas com data no passado e omite as com data no futuro
        """
        cria_pergunta(texto="Pergunta no passado", dias=-30)
        cria_pergunta(texto="Pergunta no futuro", dias=30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'],
            ['<Pergunta: Pergunta no passado (1)>']
        )

    def test_duas_perguntas_com_data_no_passado(self):
        """
        Exibe as perguntas com data no passado ordenadas (decrescentemente)
        """
        cria_pergunta(texto="Pergunta no passado 1", dias=-30)
        cria_pergunta(texto="Pergunta no passado 2", dias=-5)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'],
            ['<Pergunta: Pergunta no passado 2 (2)>',
            '<Pergunta: Pergunta no passado 1 (1)>']
        )

###
### Testes para a classe DetalhesView
#####################################
class DetalhesViewTestes(TestCase):
    def test_pergunta_no_futuro(self):
        """
        DEVE retornar um erro 404 para perguntas com data no futuro
        """
        pergunta = cria_pergunta(texto="Pergunta no futuro", dias=5)
        url = reverse('enquetes:detalhes', args=(pergunta.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):
        """
        DEVE exibir normalmente perguntas com data no passado
        """
        pergunta = cria_pergunta(texto="Pergunta no passado", dias=-5)
        url = reverse('enquetes:detalhes', args=(pergunta.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, pergunta.texto)

###
### Testes para a classe ResultadoView
######################################
class ResultadoViewTestes(TestCase):
    def test_pergunta_no_futuro(self):
        """
        DEVE retornar um erro 404 para perguntas com data no futuro
        """
        pergunta = cria_pergunta(texto="Pergunta no futuro", dias=5)
        url = reverse('enquetes:resultado', args=(pergunta.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):
        """
        DEVE exibir normalmente perguntas com data no passado
        """
        pergunta = cria_pergunta(texto="Pergunta no passado", dias=-5)
        url = reverse('enquetes:resultado', args=(pergunta.id,))
        resposta = self.client.get(url)
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, pergunta.texto)
