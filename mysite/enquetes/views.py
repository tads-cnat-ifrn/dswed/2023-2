from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views import View
from django.urls import reverse
from django.utils import timezone
from .models import Pergunta, Alternativa

class IndexView(View):
    def get(self, request, *args, **kwargs):
        lista_perguntas = Pergunta.objects.filter(
            data_pub__lte = timezone.now()
        ).order_by('-data_pub')
        contexto = {'pergunta_list': lista_perguntas}
        return render(request, 'enquetes/index.html', contexto)

class DetalhesView(View):
    def get(self, request, *args, **kwargs):
        pergunta = get_object_or_404(Pergunta, pk=kwargs['pk'])
        if pergunta.data_pub > timezone.now():
            raise Http404('Nenhuma enquete satisfaz o critério informado')
        return render(
            request, 'enquetes/detalhes.html', {'pergunta':pergunta}
        )

class ResultadoView(View):
    def get(self, request, *args, **kwargs):
        pergunta = get_object_or_404(Pergunta, pk=kwargs['pk'])
        if pergunta.data_pub > timezone.now():
            raise Http404('Nenhuma enquete satisfaz o critério informado')
        return render(request, 'enquetes/resultado.html', {'pergunta':pergunta})

class VotacaoView(View):
    def post(self, request, *args, **kwargs):
        pergunta = get_object_or_404(Pergunta, pk=kwargs['pk'])
        try:
            alt_id = request.POST['alt_id']
            alt_escolhida = pergunta.alternativa_set.get(pk=alt_id)
        except (KeyError, Alternativa.DoesNotExist):
            return self.erro_no_form(
                request, pergunta,
                'Você percisa selecionar uma alternativa válida!'
            )
        else:
            alt_escolhida.quant_votos += 1
            alt_escolhida.save()
            return HttpResponseRedirect(
                reverse('enquetes:resultado', args=(pergunta.id,))
            )
    def erro_no_form(self, request, pergunta, erro):
        contexto = {
            'pergunta': pergunta,
            'error': erro
        }
        return render(request, 'enquetes/detalhes.html', contexto)

def sobre(request):
    return HttpResponse('&copy; DSWeb/TADS/CNAT/IFRN, 2023.')

"""
1ª versão da visão de INDEX
def index(request):
    lista_perguntas = Pergunta.objects.order_by('data_pub')
    resposta = '<br/> '.join([p.texto for p in lista_perguntas])
    return HttpResponse('<h3>%s</h3>'%resposta)

from django.template import loader
2ª versão da visão de INDEX
def index(request):
    lista_perguntas = Pergunta.objects.order_by('-data_pub')
    template = loader.get_template('enquetes/index.html')
    contexto = {'lista_perguntas': lista_perguntas}
    return HttpResponse(template.render(contexto, request))

3ª versão da visão de INDEX
def index(request):
    lista_perguntas = Pergunta.objects.order_by('-data_pub')
    contexto = {'lista_perguntas': lista_perguntas}
    return render(request, 'enquetes/index.html', contexto)

from django.views import generic
4ª versão da visão de INDEX
class IndexView(generic.ListView):
    template_name = 'enquetes/index.html'
    def get_queryset(self):
        return Pergunta.objects.order_by('data_pub')

from django.http import HttpResponse, Http404
1ª versão da visão de DETALHES
def detalhes(request, pergunta_id):
    try:
        pergunta = Pergunta.objects.get(id=pergunta_id)
    except Pergunta.DoesNotExist:
        raise Http404('Nenhuma enquete satisfaz esse critério.')
    return render(request, 'enquetes/detalhes.html', {'pergunta':pergunta})

from django.shortcuts import render, get_object_or_404
2ª versão da visão de DETALHES
def detalhes(request, pergunta_id):
    pergunta = get_object_or_404(Pergunta, pk=pergunta_id)
    return render(request, 'enquetes/detalhes.html', {'pergunta':pergunta})

from django.views import View, generic
3ª versão da visão de DETALHES
class DetalhesView(generic.DetailView):
    model = Pergunta

from django.shortcuts import render, get_object_or_404
1ª versão da visão de RESULTADO
def resultado(request, pergunta_id):
    pergunta = get_object_or_404(Pergunta, pk=pergunta_id)
    return render(request, 'enquetes/resultado.html', {'pergunta':pergunta})

from django.views import View, generic
2ª versão da visão ed RESULTADO
class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/resultado.html'

"""