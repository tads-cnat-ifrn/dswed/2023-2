import datetime
from django.test import TestCase
from django.utils import timezone
from .models import Pergunta

'''
    TESTES UNITÁRIOS
'''
class PerguntaModelTest(TestCase):
    def test_publicada_recentemente_com_data_no_futuro(self):
        """
        DEVE retornar False para pergunta com data no futuro.
        """
        data = timezone.now() + datetime.timedelta(seconds=1)
        pergunta = Pergunta(data_pub = data)
        self.assertIs(pergunta.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24hs(self):
        """
        DEVE retornar False para datas anteriores a 24hs no passado.
        """
        data = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta = Pergunta(data_pub = data)
        self.assertIs(pergunta.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_dentro_de_24hs(self):
        """
        DEVE retornar True para datas dentro das ultimas 24hs.
        """
        data = timezone.now()-datetime.timedelta(hours=23,minutes=59,seconds=59)
        pergunta = Pergunta(data_pub = data)
        self.assertIs(pergunta.publicada_recentemente(), True)
