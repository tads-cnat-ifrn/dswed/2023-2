from django.contrib import admin
from .models import Pergunta, Alternativa

class AlternativaInline(admin.TabularInline):
    model = Alternativa
    extra = 2

class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['texto', 'ativo']}),
        ('Informações de Data', {'fields': ['data_pub']}),
    ]
    inlines = [AlternativaInline]
    list_display = ['texto', 'id', 'data_pub', 'publicada_recentemente']
    list_filter = ['data_pub', 'ativo']
    search_fields = ['texto']

admin.site.site_header = 'Administração DSWeb 2023.2'
admin.site.register(Pergunta, PerguntaAdmin)

